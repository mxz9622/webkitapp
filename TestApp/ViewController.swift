//
//  ViewController.swift
//  TestApp
//
//  Created by Marko Zivko on 24/10/2019.
//  Copyright © 2019 Marko Zivko. All rights reserved.
//

import UIKit
import WebKit

class ViewController: UIViewController, WKNavigationDelegate {
    
    var webView: WKWebView!
    var progressView: UIProgressView!
    
    //refactoring
    var websites = ["apple.com", "medium.com", "google.com", "amazon.com"]
    
    override func loadView() {
        
        webView = WKWebView()
        
        webView.navigationDelegate = self
        view = webView
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        if let url = URL(string: "https://\(websites[0])"){
            webView.load(URLRequest(url: url))
            webView.allowsBackForwardNavigationGestures = true
        }
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Open", style: .plain, target: self, action: #selector(openTapped))
        
        let spacer = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let refresher = UIBarButtonItem(barButtonSystemItem: .refresh, target: webView, action: #selector(webView.reload))
        
        progressView = UIProgressView(progressViewStyle: .default)
        progressView.sizeToFit()
        
        let progressButton = UIBarButtonItem(customView: progressView)
        
        toolbarItems = [progressButton, spacer, refresher]
        navigationController?.isToolbarHidden = false
        
        webView.addObserver(self, forKeyPath: #keyPath(WKWebView.estimatedProgress), options: .new, context: nil)
        
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "estimatedProgress"{
            progressView.progress = Float(webView.estimatedProgress)
        }
    }
    
    @objc func openTapped(){
        
        let ac = UIAlertController(title: "Open website", message: nil, preferredStyle: .actionSheet)
        
//        ac.addAction(UIAlertAction(title: "youtube.com", style: .default, handler: openPage))
//        ac.addAction(UIAlertAction(title: "medium.com", style: .default, handler: openPage))
//        ac.addAction(UIAlertAction(title: "apple.com", style: .default, handler: openPage))
        
        for website in websites{
            ac.addAction(UIAlertAction(title: website, style: .default, handler: openPage))
        }
        
        ac.addAction(UIAlertAction(title: "telegram.hr", style: .default, handler: openPage))
        
        ac.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        present(ac, animated: true)
        
    }
    
    func openPage(action: UIAlertAction){
        
        if let pageTitle = action.title,
            let urlPage = URL(string: "https://\(pageTitle)"){
            webView.load(URLRequest(url: urlPage))
        }
        
        print("webSite loading...")
        
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        title = webView.title
    }
    
    
    
    
}

